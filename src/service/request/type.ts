import type { AxiosRequestConfig, AxiosResponse, InternalAxiosRequestConfig } from 'axios'
// 扩展 AxiosRequestConfig类型
interface PKInterceptors<T = AxiosResponse> {
  requestSuccessFn?: (config: InternalAxiosRequestConfig) => InternalAxiosRequestConfig
  requestFailedFn?: (err: any) => any
  reponseSuccessFn?: (res: T) => T
  reponseFailedFn?: (err: any) => any
}
interface PKRequestConfig<T = AxiosResponse> extends AxiosRequestConfig {
  interceptors?: PKInterceptors<T>
}
export default PKRequestConfig
