const StoryList = () => import('@/views/StoryChat/StoryList/StoryList.vue')
export default {
  path: '/StoryChat/StoryList',
  name: 'StoryList',
  component: StoryList,
  children: []
}
