const ChatStory = () => import('@/views/StoryChat/ChatStory/ChatStory.vue')
export default {
  path: '/StoryChat/ChatStory',
  name: 'ChatStory',
  component: ChatStory,
  children: []
}
